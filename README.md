# Meteoro - Kubernetes
## Ambiente para estudos sobre:
1. Kubernetes
2. Ansible
3. CI/CD

## Dependências:
1. Vagrant

## Como usar?
Clone o repositorio e start todas as máquinas. Este processo ainda é manual. Edite as Vagrantfiles e coloque a sua chave ssh provavelmente é a linha 79 do arquivo - siga o exemplo.

> vagrant up
> vagrant provision

~-- Pilhado ao extremo em aprender - cflb