# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  config.vm.box = "bento/ubuntu-20.04"

  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # NOTE: This will enable public access to the opened port
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine and only allow access
  # via 127.0.0.1 to disable public access
  # config.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
   config.vm.hostname = "k8-manager"
   config.vm.network "private_network", ip: "172.1.1.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
   config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
   config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
     vb.memory = "4024"
   end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Enable provisioning with a shell script. Additional provisioners such as
  # Ansible, Chef, Docker, Puppet and Salt are also available. Please see the
  # documentation for more information about their specific syntax and use.
   config.vm.provision "shell", inline: <<-SHELL
      curl -fsSL https://get.docker.com | bash
      apt-get update && apt-get install -y apt-transport-https
      curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
      echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list
      apt-get update
      apt-get install -y kubelet kubeadm kubectl
  #   apt-get install -y apache2
      sed -i "s/cgroup-driver=systemd/cgroup-driver=cgroupfs/g" /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
      systemctl daemon-reload
      systemctl restart kubelet
      swapoff -a
      echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDLDv2DOSxLNsv9prD187ydkSjZ5Be/SdFoKlMoCDwjGrhMu0Cs4Pl1go0GwFOwVO+DhojBUJU4lJupQdSZ3BLKsC1bOLiPlTLQ6ff20ulJoCRoZO/T16/fHhEnvQsTO5HRcxRXL2PNGQrbxNwQfgZIU8RnwU89JMxk6S985H6pHrWnHviMV2XK+0hHXdh2AVim3h7mOAO2AuiqY1kkV+GSm5AHDEY5NZARvg80dDYmVJEEvvpGx0SMJ/bYvv2cBAPIDleuTfYnrhtTIDua+i3HmuvT401fZry15fQfx2LGhQEzuuhbngsmlvXHg9W+cktik8M1+pwVKv8BPP9LunOZo4efHlwL35ExRgdd72MCLVFHCbuzt9tIJ6YRh7GCbMR8GqQ8px1oF0PpBGMAuoWhWD5MTbHvhtfYBRZ6qZJHPXBhw8xHD3dZQwAqnv74pPgczM3CmhG6XOcclr3BfbgYGG5OSdfdQ43QEZpoZlcqtF1VkGOQOv0P/Omt9U7KgkXcGMMtf8PHki59e/jShKhom3Unjhfhj0NrihQ44T2q0BOJw8SQZdxDzGq2IhR666ho3WOpcLRn5OhCZ3bN/d2CXseVPMBK0mtkWaaoDczWEpYxN1Jzx1O6/cOKf4SKNPfezHB34eZDKbibkLYwpfUVjnaX838ODebnP5CKYt2GvQ== cflb@bacteria" >> /home/vagrant/.ssh/authorized_keys
   SHELL
end
